<?php
/*
 Plugin Name: WooCommerce InvoiceXpress Extension
Plugin URI: http://woothemes.com/woocommerce
Description: Automatically create InvoiceXpress invoices when sales are made.
Version: 0.9
Author: WidgiLabs
Author URI: http://www.widgilabs.com
License: GPLv2
*/

/**
 * Required functions
 **/
if ( ! function_exists( 'is_woocommerce_active' ) ) require_once( 'woo-includes/woo-functions.php' );


if (is_woocommerce_active()) {
	
	add_action('plugins_loaded', 'woocommerce_invoicexpress_init', 0);
	function woocommerce_invoicexpress_init() {
		$woocommerce_invoicexpress = new woocommerce_invoicexpress;
		require_once( 'woo-includes/contact-mail-setup.php' );
	}

	add_action('init', 'localization_init');
	function localization_init() {
	    $path = dirname(plugin_basename( __FILE__ )) . '/languages/';
	    $loaded = load_plugin_textdomain( 'wc_invoicexpress', false, $path);
	    if ($_GET['page'] == basename(__FILE__) && !$loaded) {          
	        return;
	    } 
	} 


	class woocommerce_invoicexpress {
		function __construct() {
			require_once('InvoiceXpressRequest-PHP-API/lib/InvoiceXpressRequest.php');

			$this->subdomain 	= get_option('wc_ie_subdomain');
			$this->token 		= get_option('wc_ie_api_token');
	
			add_action('admin_init',array(&$this,'settings_init'));
			add_action('admin_menu',array(&$this,'menu'));
			//add_action('woocommerce_checkout_order_processed',array(&$this,'process')); // Check if user is InvoiceXpress client (create if not) and create invoice.

			switch ( get_option( 'wc_ie_order_state' ) ) {
				case 'processing':
					add_action('woocommerce_order_status_processing',array(&$this,'process'));
					break;
				case 'completed':
					add_action('woocommerce_order_status_completed',array(&$this,'process'));
					break;
				case 'on-hold':
					add_action('woocommerce_order_status_on-hold',array(&$this,'process'));
					break;
			}

			add_action('woocommerce_order_actions', array(&$this,'my_woocommerce_order_actions'), 10, 1);
			add_action('woocommerce_order_action_my_action', array(&$this,'do_my_action'), 10, 1);

		}



		function my_woocommerce_order_actions($actions) {
			$actions['my_action'] = "Create Invoice (InvoiceXpress)";
			return $actions;
		}
		

		function do_my_action($order) {
			// Do something here with the WooCommerce $order object
			$this->process($order->id);
		
		}

		function menu() {
			add_submenu_page('woocommerce', __('InvoiceXpress', 'wc_invoicexpress'),  __('InvoiceXpress', 'wc_invoicexpress') , 'manage_woocommerce', 'woocommerce_invoicexpress', array(&$this,'options_page'));
		}


		function settings_init() {
			global $woocommerce;

			wp_enqueue_style('woocommerce_admin_styles', $woocommerce->plugin_url().'/assets/css/admin.css');

			$general_settings = array(
				array(
					'name'		=> 'wc_ie_settings',
					'title' 	=> __('InvoiceXpress for WooCommerce General Settings','wc_invoicexpress'),
					'page'		=> 'woocommerce_invoicexpress_general',
					'settings'	=> array(
							array(
									'name'		=> 'wc_ie_subdomain',
									'title'		=> __('Subdomain','wc_invoicexpress'),
							),
							array(
									'name'		=> 'wc_ie_api_token',
									'title'		=> __('API Token','wc_invoicexpress'),
							),
							array(
									'name'		=> 'wc_ie_create_invoice',
									'title'		=> __('Create Invoice','wc_invoicexpress'),
							),
							array(
									'name'		=> 'wc_ie_send_invoice',
									'title'		=> __('Send Invoice','wc_invoicexpress'),
							),
							array(
									'name'		=> 'wc_ie_create_simplified_invoice',
									'title'		=> __('Create Simplified Invoice','wc_invoicexpress'),
							),
							array(
									'name'		=> 'wc_ie_iva_checkbox',
									'title'		=> __('Isenção IVA Art 53','wc_invoicexpress'),
							),
							array(
									'name'		=> 'wc_ie_invoice_draft',
									'title'		=> __('Invoice as Draft','wc_invoicexpress'),
							),
							array(
									'name'		=> 'wc_ie_order_state',
									'title'		=> __('Order State','wc_invoicexpress'),
							),
						),
					),
				);

			$email_settings = array(
				array(
					'name'		=> 'wc_ie_settings',
					'title' 	=> __('InvoiceXpress for WooCommerce Email Settings','wc_invoicexpress'),
					'page'		=> 'woocommerce_invoicexpress_email',
					'settings'	=> array(
						array(
								'name'		=> 'wc_ie_email_subject',
								'title'		=> __('Email Subject','wc_invoicexpress'),
						),
						array(
								'name'		=> 'wc_ie_email_body',
								'title'		=> __('Email Body','wc_invoicexpress'),
						),
					)
				)
			);

			foreach($general_settings as $sections=>$section) {
				add_settings_section($section['name'],$section['title'],array(&$this,$section['name']),$section['page']);
				foreach($section['settings'] as $setting=>$option) {
					add_settings_field($option['name'],$option['title'],array(&$this,$option['name']),$section['page'],$section['name']);
					register_setting($section['page'],$option['name']);
					$this->$option['name'] = get_option($option['name']);
				}
			}

			foreach($email_settings as $sections=>$section) {
				add_settings_section($section['name'],$section['title'],array(&$this,$section['name']),$section['page']);
				foreach($section['settings'] as $setting=>$option) {
					add_settings_field($option['name'],$option['title'],array(&$this,$option['name']),$section['page'],$section['name']);
					register_setting($section['page'],$option['name']);
					$this->$option['name'] = get_option($option['name']);
				}
			}


		}

		function wc_ie_tabs( $current = 'general' ){

			$tabs = array(  
				'general'   => __( 'General', 'wc_invoicexpress' ),
				'email'     => __( 'E-Mail', 'wc_invoicexpress' ),
				'support'   => __( 'Support', 'wc_invoicexpress' )
			);

			echo '<div id="icon-themes" class="icon32"><br></div>';
			echo '<h2 class="nav-tab-wrapper">';

			foreach ( $tabs as $tab => $name ) {
				$class = ( $tab == $current ) ? ' nav-tab-active' : '';

				echo '<a class="nav-tab'.$class.'" href="?page=woocommerce_invoicexpress&tab='.$tab.'">'.$name.'</a>';
			}

			echo '</h2>';
		}


		function options_page() { 
			global $pagenow;

			if( $pagenow == 'admin.php' && $_GET["page"] == 'woocommerce_invoicexpress' ){
			?>
				<div class="wrap woocommerce">
				<form method="post" id="mainform" action="options.php">
			<?php
				if ( isset ( $_GET['tab'] ) ) $this->wc_ie_tabs($_GET['tab']); else $this->wc_ie_tabs('general');

				$tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'general';

				switch ( $tab ) {
					case 'general':
					?>
						<div class="icon32 icon32-woocommerce-settings" id="icon-woocommerce"><br /></div>
						<h2><?php _e('InvoiceXpress for WooCommerce','wc_invoicexpress'); ?></h2>
						<?php settings_fields('woocommerce_invoicexpress_general'); ?>
						<?php do_settings_sections('woocommerce_invoicexpress_general'); ?>
						<p class="submit"><input type="submit" class="button-primary" value="<?php _e( 'Save Changes', 'wc_invoicexpress' ) ?>" /></p>
						</form>
						</div>
					<?php
						break;
					case 'email':
					?>
						<div class="icon32 icon32-woocommerce-settings" id="icon-woocommerce"><br /></div>
						<h2><?php _e('InvoiceXpress for WooCommerce','wc_invoicexpress'); ?></h2>
						<?php settings_fields('woocommerce_invoicexpress_email'); ?>
						<?php do_settings_sections('woocommerce_invoicexpress_email'); ?>
						<p class="submit"><input type="submit" class="button-primary" value="<?php _e( 'Save Changes', 'wc_invoicexpress' ) ?>" /></p>
						</form>
						</div>
					<?php
						break;
				}

				if ( $tab == 'support' ){ $this->wc_ie_support_form(); }

			}

		}
		//wc_invoicexpress
		function wc_ie_settings() {
			echo '<p>'.__('Please fill in the necessary settings below. InvoiceXpress for WooCommerce works by creating an invoice when order status is updated to processing.','wc_invoicexpress').'</p>';
		}
		function wc_ie_subdomain() {
			echo '<input type="text" name="wc_ie_subdomain" id="wc_ie_subdomain" value="'.get_option('wc_ie_subdomain').'" />';
			echo ' <label for="wc_ie_subdomain">'.__( 'When you access InvoiceXpress you use https://<b>subdomain</b>.app.invoicexpress.com ( <a href="http://widgilabs.bitbucket.org/static/invoicexpress_subdomain_faq.html" target="_blank">Help me find my subdomain</a> )', 'wc_invoicexpress' ).'</label>';
		}
		function wc_ie_api_token() {
			echo '<input type="password" name="wc_ie_api_token" id="wc_ie_api_token" value="'.get_option('wc_ie_api_token').'" />';
			echo ' <label for="wc_ie_api_token">'.__( 'Go to Settings >> API in InvoiceXpress to get one.', 'wc_invoicexpress' ).'</label>';
		}
		function wc_ie_create_invoice() {
			$checked = (get_option('wc_ie_create_invoice')==1) ? 'checked="checked"' : '';
			echo '<input type="hidden" name="wc_ie_create_invoice" value="0" />';
			echo '<input type="checkbox" name="wc_ie_create_invoice" id="wc_ie_create_invoice" value="1" '.$checked.' />';
			echo ' <label for="wc_ie_create_invoice">'.__( 'Create invoices for orders that come in, otherwise only the client is created (<i>recommended</i>).', 'wc_invoicexpress' ).'</label>';
		}
		function wc_ie_send_invoice() {
			$checked = (get_option('wc_ie_send_invoice')==1) ? 'checked="checked"' : '';
			echo '<input type="hidden" name="wc_ie_send_invoice" value="0" />';
			echo '<input type="checkbox" name="wc_ie_send_invoice" id="wc_ie_send_invoice" value="1" '.$checked.' />';
			echo ' <label for="wc_ie_send_invoice">'.__( 'Send the client an e-mail with the order invoice attached (<i>recommended</i>).', 'wc_invoicexpress' ).'</label>';
		}

		function wc_ie_create_simplified_invoice() {
			$checked = (get_option('wc_ie_create_simplified_invoice')==1) ? 'checked="checked"' : '';
			echo '<input type="hidden" name="wc_ie_create_simplified_invoice" value="0" />';
			echo '<input type="checkbox" name="wc_ie_create_simplified_invoice" id="wc_ie_create_simplified_invoice" value="1" '.$checked.' />';
			echo ' <label for="wc_ie_create_simplified_invoice">'.__( 'Create simplified invoices. Only available for Portuguese accounts.', 'wc_invoicexpress' ).'</label>';
		}

		function wc_ie_iva_checkbox(){
			$checked = (get_option('wc_ie_iva_checkbox')==1) ? 'checked="checked"' : '';
			echo '<input type="hidden" name="wc_ie_iva_checkbox" value="0" />';
			echo '<input type="checkbox" name="wc_ie_iva_checkbox" id="wc_ie_iva_checkbox" value="1" '.$checked.' />';
			echo ' <label for="wc_ie_iva_checkbox">'.__( 'Isenção IVA Art 53', 'wc_invoicexpress' ).'</label>';
		}

		function wc_ie_invoice_draft(){
			$checked = (get_option('wc_ie_invoice_draft')==1) ? 'checked="checked"' : '';
			echo '<input type="hidden" name="wc_ie_invoice_draft" value="0" />';
			echo '<input type="checkbox" name="wc_ie_invoice_draft" id="wc_ie_invoice_draft" value="1" '.$checked.' />';
			echo ' <label for="wc_ie_invoice_draft">'.__( 'Create invoice as draft.', 'wc_invoicexpress' ).'</label>';
		}

		function wc_ie_order_state(){
			$states = array( 
				'processing' => 'Processing',
				'completed'  => 'Completed',
				'on-hold'    => 'On Hold'
			);

			echo '<input type="hidden" name="wc_ie_order_state" value="0" />';
			echo '<select name="wc_ie_order_state" >';

			foreach ( $states as $key => $value ) {

				$selected = ( get_option( 'wc_ie_order_state' ) == $key ) ? 'selected' : '';

				echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';

			}

			echo '</select>';
			echo ' <label for="wc_ie_invoice_draft">'.__( 'The state in which the order must be to send the invoice.', 'wc_invoicexpress' ).'</label>';
		
		}


		// email tab
		function wc_ie_email_subject(){
			echo '<input type="text" placeholder="Order Invoice" name="wc_ie_email_subject" id="wc_ie_email_subject" value="'.get_option('wc_ie_email_subject').'" />';
			echo ' <label for="wc_ie_email_subject">'._e( ' The email subject.', 'wc_invoicexpress' ).'</label></br>';
		}

		function wc_ie_email_body(){
			echo '<textarea style="resize:none" placeholder="Please find your invoice in attach. Archive this e-mail as proof of payment." rows="7" cols="60" name="wc_ie_email_body" id="wc_ie_email_textarea" >'.get_option('wc_ie_email_body').'</textarea>';
			echo ' <label for="wc_ie_email_body">'._e( ' The email body.', 'wc_invoicexpress' ).'</label>';
		}

		// support tab
		function wc_ie_support_form(){
			$user_name = wp_get_current_user()->display_name;
			$user_email = wp_get_current_user()->user_email;
		?>
			<div class="wrap woocommerce">
				<?php 

					if( isset($_GET['status']) ){
						switch ( $_GET['status'] ) {
							case 'success':
								echo "<div id='message' class='updated'><p><strong>";
								echo __( 'Message successfully sent! We will get back to you soon ;)','wc_invoicexpress' );
								echo "</strong></p></div>";
								break;
							case 'failure':
								echo "<div id='message' class='error'><p><strong>";
								echo __( 'Oops! Something went wrong :(','wc_invoicexpress' );
								echo "</strong></p></div>";
								break;
							case 'error':
								echo '<p>'.__( 'Please fill in all the fields bellow','wc_invoicexpress' ).'<p>';
								break;
						}
					}

				?>
				<div class="icon32 icon32-woocommerce-settings" id="icon-woocommerce"><br /></div>
				<h2><?php _e("InvoiceXpress for WooCommerce","wc_invoicexpress") ?></h2>
				<h3><?php _e('Contact Us','wc_invoicexpress') ?></h3>
				<p><?php _e('This will open a ticket with WidgiLabs Support.<br> Our team will review your request and will send you a response to the e-mail "<b>'.$user_email.'</b>". (usually within 24 hours).','wc_invoicexpress') ?></p>

				<form action="<?php echo $_SERVER['PHP_SELF']; ?>" class="wc-ie-contact-form" id="wc-ie-contact-form" method="post">
					<ul>
						<li>
							<label for="contactSubject">Subject:</label><br>
							<input type="text" name="contactSubject" id="contactSubject" value="" placeholder="Subject" />
							<span class="error-msg" id="sub-error-msg" style="color:red"></span>
						</li>
						<li>
							<label for="commentsText">Message Body:</label><br>
							<textarea placeholder="Your comments" name="commentsText" id="commentsText" rows="7" cols="60" style="resize:none"></textarea>
							<span class="com-error-msg" id="com-error-msg" style="color:red"></span>
						</li>
						<li>
							<button type="submit" id="form-submit"><?php _e( 'Send Message', 'wc_invoicexpress' ) ?></button>
						</li>
					</ul>
					<input type="hidden" name="submitted" id="submitted" value="true" />
					<input type="hidden" name="user_name" id="form_user_name" value="<?php echo $user_name ?>" />
					<input type="hidden" name="user_email" id="form_user_email" value="<?php echo $user_email ?>" />
				</form>
			</div>
		<?php
		}


		function process($order_id) {

			InvoiceXpressRequest::init($this->subdomain, $this->token);

			$order = new WC_Order($order_id);

			$user_id = $order->get_user()->data->ID;

			$client_name = $order->billing_first_name." ".$order->billing_last_name;

			$countries = new WC_Countries();
			$countries_list = $countries->countries;
			$country = $countries_list[$order->billing_country];

			$vat='';
			$vat_text = '';
			$user_details = get_user_by( 'id',$user_id );
			$client_email = $order->billing_email;

			if(isset($user_details->billing_nif)){
				$vat = $user_details->billing_nif;
			}

			$vat = apply_filters( 'wc_ie_change_billing_nif', $vat );
			if ( $vat ){
				$vat_text = ' - NIF: '.$vat;
			}

			// Lets get the user's InvoiceXpress data
			$client_data = array(
								'name' => $client_name. " (".$client_email.")".$vat_text,
								'email' => $client_email,
								'phone' => $order->billing_phone,
								'address' => $order->billing_address_1."\n".$order->billing_address_2."\n", 
								'postal_code' => $order->billing_postcode . " - " . $order->billing_city,
								'country' => $country,
								'fiscal_id' => $vat,
								'send_options' => 3
								);
		
			if(get_option('wc_ie_create_invoice')==1) {
				foreach($order->get_items() as $item) {						
					
					$pid = $item['item_meta']['_product_id'][0];
					
					$prod = get_product($pid);

					$original_price = floatval( $item['line_subtotal'] );
					$discount_price = floatval( $item['line_total'] );

					$discount_value = floatval( ( ( $original_price - $discount_price ) / $original_price ) * 100 );
					$discount_value = number_format( $discount_value, 2 );

					$iva = 1.23;
					$iva_name = 'IVA23';
					if ( get_option( 'wc_ie_iva_checkbox' ) == 1 ){
						$iva = 1;
						$iva_name = 'IVA0';

					}

					$items[] = array(
							'name'			=> "#".$pid, //$item['name'],
							'description'	=> get_the_title($pid), //'('.$item['qty'].') '.$item['name'],
							'unit_price'	=> $prod->price / $iva, // subtract tax 23%
							'quantity'		=> $item['qty'],
							'discount'		=> $discount_value,
							'unit'			=> 'unit',
							'tax'			=> array(
								'name'	=> $iva_name
							)
					);
				}
				// divide the value of shipping per 1.23 so that when
				// invoicexpress calculates the tax is correct
				$shipping_unit_price = 	$order->get_total_shipping() / $iva;
				$items[] = array(
						'name'			=> 'Envio',
						'description'	=> 'Custos de Envio',
						'unit_price'	=> $shipping_unit_price,
						'quantity'		=> 1,
						'tax'			=> array(
							'name'	=> $iva_name
						)
				);
				
				if(get_option('wc_ie_create_simplified_invoice')==1) {
					$data = array(
							'simplified_invoice' => array(
									'date'	=> $order->completed_date,
									'due_date' => $order->completed_date,
									'client' => $client_data,
									'reference' => $order_id,
									'items'		=> array(
											'item'	=> $items
									)
							)
					);

					if( get_option( 'wc_ie_iva_checkbox' ) == 1 ){
						$data['simplified_invoice']['tax_exemption'] = 'M10';
					}

				} else {
					$data = array(
							'invoice' => array(
									'date'	=> $order->completed_date,
									'due_date' => $order->completed_date,
									'client' => $client_data,
									'reference' => $order_id,
									'items'		=> array(
											'item'	=> $items
									)
							)
					);

					if( get_option( 'wc_ie_iva_checkbox' ) == 1 ){
						$data['invoice']['tax_exemption'] = 'M10';
					}

				}

				if(get_option('wc_ie_create_simplified_invoice')==1) {
					$invoice = new InvoiceXpressRequest('simplified_invoices.create');
				} else {
					$invoice = new InvoiceXpressRequest('invoices.create');
				}

				$invoice->post($data);
				$invoice->request();
				if($invoice->success()) {
					$response = $invoice->getResponse();
					$invoice_id = $response['id'];
					$order->add_order_note(__('Client invoice in InvoiceXpress','wc_invoicexpress').' #'.$invoice_id);
					add_post_meta($order_id, 'wc_ie_inv_num', $invoice_id, true);
					
					// extra request to change status to final
					if ( get_option( 'wc_ie_invoice_draft' ) == 0 ){
						if(get_option('wc_ie_create_simplified_invoice')==1) {
							$invoice = new InvoiceXpressRequest('simplified_invoices.change-state');
						} else {
							$invoice = new InvoiceXpressRequest('invoices.change-state');
						}
						$data = array('invoice' => array('state'	=> 'finalized'));
						$invoice->post($data);
						$invoice->request($invoice_id);
						
						if($invoice->success()) { // keep the invoice sequence number in a meta
							$response = $invoice->getResponse();
							$inv_seq_number = $response['sequence_number'];
							add_post_meta($order_id, 'wc_ie_inv_seq_num', $inv_seq_number, true);
						}
						
						$data = array('invoice' => array('state'	=> 'settled'));
						$invoice->post($data);
						$invoice->request($invoice_id);
					}

				} else {
					$error = $invoice->getError();

					if (is_array($error)) {
						$order->add_order_note(__('InvoiceXpress Invoice API Error:', 'wc_invoicexpress').': '.print_r($error, true));						
					} else {
						$order->add_order_note(__('InvoiceXpress Invoice API Error:', 'wc_invoicexpress').': '.$error);
					}

				}
			}
			
			if(get_option('wc_ie_send_invoice')==1 && isset($invoice_id)) {

				$subject = get_option('wc_ie_email_subject') ? get_option('wc_ie_email_subject') : __('Order Invoice','wc_invoicexpress');
				$body = get_option('wc_ie_email_body') ? get_option('wc_ie_email_body') : __('Please find your invoice in attach. Archive this e-mail as proof of payment.','wc_invoicexpress');

				$data = array(
						'message' => array(
								'client' => array(
										'email' => $order->billing_email,
										'save' => 1
										),
								'subject' => $subject,
								'body' => $body
								)
						);
	
				if(get_option('wc_ie_create_simplified_invoice')==1) {
					$send_invoice = new InvoiceXpressRequest('simplified_invoices.email-invoice');
				} else {
					$send_invoice = new InvoiceXpressRequest('invoices.email-invoice');
				}
				$send_invoice->post($data);
				$send_invoice->request($invoice_id);
				
				if($send_invoice->success()) {
					$response = $send_invoice->getResponse();
					$order->add_order_note(__('Client invoice sent from InvoiceXpress','wc_invoicexpress'));
				} else {
					$order->add_order_note(__('InvoiceXpress Send Invoice API Error','wc_invoicexpress').': '.$send_invoice->getError());
				}
			}
			
		}
	}
}